let getPoints triangle = triangle.p1, triangle.p2, triangle.p3;; 

let createSquareMat size init = Array.make_matrix size size init;;

let getCell i j matrix = matrix.(i).(j);;
let setCell i j x matrix = matrix.(i).(j) <- x;;

let randSquareMat size bound =
  let randMat = createSquareMat size 0 in
  for i = 0 to size - 1 do
    for j = 0 to size - 1 do
      let rand = Random.int bound in
      setCell i j rand randMat
    done;
  done;
  randMat
;;

let getSize matrix =
  let dimX = Array.length matrix and
      dimY = Array.length matrix.(0) in
  if dimX = dimY then dimX
  else failwith "getSize"
;;

(* returns the sub-matrix of matrix without
   the line i and the column j. *)
let extractMat i j matrix =
  let n = getSize matrix in
  let newMatrix = createSquareMat (n-1) 0. in
  for k = 0 to n - 1 do
    for l = 0 to n - 1 do
      let x = getCell k l matrix in
      if (k < i) && (l < j) then
	setCell k l x newMatrix
      else if (k < i) && (l > j) then
	setCell k (l-1) x newMatrix
      else if (k > i) && (l < j) then
	setCell (k-1) l x newMatrix
      else if (k > i) && (l > j) then
	setCell (k-1) (l-1) x newMatrix
      else ()
    done;
  done;
  newMatrix
;;

(* Calculates determinant using expansion by minors *)
let rec detMat matrix =
  let n = getSize matrix in
  if n = 2 then
    (getCell 0 0 matrix) *. (getCell 1 1 matrix)
    -. (getCell 1 0 matrix) *. (getCell 0 1 matrix)
  else
    let sum = ref 0. in
    for i = 0 to n - 1 do
      let parity = if (i mod 2) = 0 then 1. else -1. in
      sum := !sum +. parity *. (getCell i 0 matrix)
      *. detMat (extractMat i 0 matrix)
    done;
    !sum
;;
      
let ccw p1 p2 p3 =
  let ccwTestMat = createSquareMat 2 0. in
  ccwTestMat.(0).(0) <- p2.x -. p1.x;
  ccwTestMat.(1).(0) <- p2.y -. p1.y;
  ccwTestMat.(0).(1) <- p3.x -. p1.x;
  ccwTestMat.(1).(1) <- p3.y -. p1.y;
  let result = detMat ccwTestMat in
  if result == 0. then failwith "ccw"
  else result > 0.
;;

let inCircle tri pt =
  let p1, p2, p3 = getPoints tri and
      circleTestMat = createSquareMat 4 1. in
  setCell 0 0 (p1.x) circleTestMat;
  setCell 1 0 (p2.x) circleTestMat;
  setCell 2 0 (p3.x) circleTestMat;
  setCell 3 0 (pt.x) circleTestMat;
  
  setCell 0 1 (p1.y) circleTestMat;
  setCell 1 1 (p2.y) circleTestMat;
  setCell 2 1 (p3.y) circleTestMat;
  setCell 3 1 (pt.y) circleTestMat;
  
  setCell 0 2 (p1.x ** 2. +. p1.y ** 2.) circleTestMat;
  setCell 1 2 (p2.x ** 2. +. p2.y ** 2.) circleTestMat;
  setCell 2 2 (p3.x ** 2. +. p3.y ** 2.) circleTestMat;
  setCell 3 2 (pt.x ** 2. +. pt.y ** 2.) circleTestMat;

  let det = detMat circleTestMat in
  if det == 0. then failwith "inCircle"
  else
    (
      if ccw p1 p2 p3 then det > 0.
      else det < 0.
    )
;;
