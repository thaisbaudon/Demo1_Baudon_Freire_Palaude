module Geometry

=

struct
  type point = {x : float; y : float}
  type pointSet = point list
      
  let makePoint (x:float) (y:float) = {x = x; y = y}

  
  let emptyPoint () = ([] : pointSet)
  let isEmptyPoint (u : pointSet) =
    match u with
      | [] -> true
      | _ -> false
  let carPoint (u : pointSet) =
    match u with
    | [] -> failwith "car"
    | x :: _ -> x
  let cdrPoint (u : pointSet) =
    match u with
    | [] -> failwith "cdr"
    | _ :: xs -> (xs : pointSet)

  let consPoint (x : point) (u : pointSet) = (x :: u : pointSet)

  (*###########################################################*)
  
  type edge = {p1: point; p2: point}
  type edgeSet = edge list

  let emptyEdge () = ([] : edgeSet)
  let isEmptyEdge (u : edgeSet) =
    match u with
      | [] -> true
      | _ -> false
  let carEdge (u : edgeSet) =
    match u with
    | [] -> failwith "car"
    | x :: _ -> x
  let cdrEdge (u : edgeSet) =
    match u with
    | [] -> failwith "cdr"
    | _ :: xs -> xs

  let consEdge (x : edge) (u : edgeSet) = x :: u


  let makeEdge (pt1:point) (pt2:point) = {p1 = pt1; p2 = pt2}

  let getPointsFromEdge e = e.p1, e.p2
                                    
  let equals (e1:edge) (e2:edge) =
    e1.p1 = e2.p1 && e1.p2 = e2.p2
    || e1.p2 = e2.p1 && e1.p1 = e2.p2

  let rec removeFromList (x:edge) (l:edge list) =
    if isEmptyEdge l then l
    else let y = carEdge l and m = cdrEdge l in
      if equals x y then removeFromList x m
      else consEdge y (removeFromList x m)
          
  let rec isInList (x:edge) (l:edge list) =
    if isEmptyEdge l then false
    else let y = carEdge l and m = cdrEdge l in
      equals x y || isInList x m

  (*###########################################################*)
  
  type triangle = {p1 : point; p2 : point; p3 : point}
  type triangleSet = triangle list

  let makeTriangle (p1:point) (p2:point) (p3:point) =
    {p1 = p1; p2 = p2; p3 = p3}
    
  let getPoints (tri:triangle) = tri.p1, tri.p2, tri.p3

  let getEdgesFromTriangle (t:triangle) =
    [|makeEdge t.p1 t.p2; makeEdge t.p2 t.p3; makeEdge t.p3 t.p1|]

  let makeTriangleFromEdge (e:edge) (p3:point) =
    let p1, p2 = getPointsFromEdge e in makeTriangle p1 p2 p3

  let emptyTriangle () = ([] : triangleSet)
  let isEmptyTriangle (u : triangleSet) =
    match u with
      | [] -> true
      | _ -> false
  let carTriangle (u : triangleSet) =
    match u with
    | [] -> failwith "car"
    | x :: _ -> x
  let cdrTriangle (u : triangleSet) =
    match u with
    | [] -> failwith "cdr"
    | _ :: xs -> (xs : triangleSet)

  let consTriangle (x : triangle) (u : triangleSet) = (x :: u : triangleSet)
end
;;
