let drawOnePoint point =
	let intPointX = int_of_float point.x
	and intPointY = int_of_float point.y 
	in	plot intPointX intPointY
;;

let drawPoints points = 
	List.iter drawOnePoint points
;;

let drawOneTriangle triangle = 
	let point1 = triangle.p1
	and point2 = triangle.p2
	and point3 = triangle.p3
	in
		let intPoint1X = int_of_float point1.x
		and intPoint1Y = int_of_float point1.y
		and intPoint2X = int_of_float point2.x
		and intPoint2Y = int_of_float point2.y
		and intPoint3X = int_of_float point3.x
		and intPoint3Y = int_of_float point3.y 
		in	moveto intPoint1X intPoint1Y;
			lineto intPoint2X intPoint2Y;
			lineto intPoint3X intPoint3Y;
			lineto intPoint1X intPoint1Y
;;

let drawTriangles triangles = 
	List.iter drawOneTriangle triangles
;;
