let printPoint pt =
  print_string "(";
  print_float pt.x;
  print_string ", ";
  print_float pt.y;
  print_string ")"
;;

let rec printPointSet ptSet =
  if isEmptyPoint (cdrPoint ptSet) then
    (
      printPoint (carPoint ptSet);
      print_newline ()
    )
  else
    (
      printPoint (carPoint ptSet);
      print_string "; ";
      printPointSet (cdrPoint ptSet)
    )
;;

let randomPoints nb maxX maxY = 
	let rec randomPointsAux nb pointsList = match nb with
	|0 -> pointsList
	|_ -> let (newPoint:point) = {x=Random.float maxX; y=Random.float maxY} in
			randomPointsAux (nb-1) (consPoint newPoint pointsList)
	in
	randomPointsAux nb ([]:pointSet)
	;;
