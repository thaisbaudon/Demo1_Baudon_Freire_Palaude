#load "graphics.cma";;
open Graphics;;
#use "types.ml";;
open Geometry;;
#use "matrix.ml";;
#use "createPoints.ml";;
#use "drawPointsTriangles.ml";;
#use "initialDelaunay.ml";;
#use "testccw.ml";;
#use "testInCircle.ml";;

close_graph();;
open_graph " 801x601-0+0";;

let demonstration = let input = ref 'a' in
  printPointSet (randomPoints 50 800. 600.) ;
  input := read_key ();
  initialDelaunay (randomPoints 50 800. 600.) 800. 600.;
  input := read_key ();
  testccw ();
  input := read_key ();
  testInCircle ();
  input := read_key ()
;;

demonstration;;
