let printBool b =
  if b then print_string "true"
  else print_string "false";
  print_newline ()
;;

let printPoint pt =
  print_string "(";
  print_float pt.x;
  print_string ", ";
  print_float pt.y;
  print_string ")\n"
;;

let testccw () =
  let ptDir0 = makePoint 0. 0. in
  let ptDir1 = makePoint 800. 0. in
  let ptDir2 = makePoint 0. 600. in
  let ptIndir0 = makePoint 0. 0. in
  let ptIndir1 = makePoint 0. 600. in
  let ptIndir2 = makePoint 800. 0. in
  let bIndir = ccw ptIndir0 ptIndir1 ptIndir2 in
  let bDir = ccw ptDir0 ptDir1 ptDir2 in
  print_string "\nTEST FUNCTION : ccw\n";
  print_string "FIRST POINT: INDIRECT\n";
  printPoint ptIndir0;
  printPoint ptIndir1;
  printPoint ptIndir2;
  printBool bIndir;
  print_string "SECOND POINT: DIRECT\n";
  printPoint ptDir0;
  printPoint ptDir1;
  printPoint ptDir2;
  printBool bDir
;;
