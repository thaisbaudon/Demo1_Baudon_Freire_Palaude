let printBool b =
  if b then print_string "true"
  else print_string "false";
  print_newline ()
;;

let printPoint pt =
  print_string "(";
  print_float pt.x;
  print_string ", ";
  print_float pt.y;
  print_string ")\n"
;;

let testInCircle () =
  let pt0 = makePoint 0. 0. in
  let pt1 = makePoint 800. 0. in
  let pt2 = makePoint 0. 600. in
  let tri = makeTriangle pt0 pt1 pt2 in
  let ptExt = makePoint 800. 600. in
  let ptIns = makePoint 100. 100. in
  let bExt = inCircle tri ptExt in
  let bIns = inCircle tri ptIns in
  print_string "\nTEST FUNCTION : InCircle\n";
  print_string "TESTED TRIANGLE:\n";
  printPoint pt0;
  printPoint pt1;
  printPoint pt2;
  print_string "FIRST POINT: INSIDE\n";
  printPoint ptIns;
  printBool bIns;
  print_string "SECOND POINT: OUTSIDE\n";
  printPoint ptExt;
  printBool bExt;
  print_newline ()
;;
