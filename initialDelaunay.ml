let initialTriangles maxX maxY = 
	let point00 = {x = 0.0 ; y = 0.0}
	and point0Max = {x = 0.0 ; y = maxY}
	and pointMax0 = {x = maxX ; y = 0.0}
	and pointMaxMax = {x = maxX ; y = maxY} in
		let triangle1= {p1= point0Max; p2= pointMax0; p3= point00}
		and triangle2= {p1= point0Max; p2= pointMax0; p3= pointMaxMax} in
			[triangle1; triangle2]
;;

let initialDelaunay points maxX maxY = 
	let triangles = initialTriangles maxX maxY in
		drawTriangles triangles;
		drawPoints points
